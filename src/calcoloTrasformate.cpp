#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <std_msgs/String.h>
#include <turtle_ur_kin/AprilTagDetection.h>
#include <turtle_ur_kin/AprilTagDetectionArray.h>
#include <tf/transform_listener.h>
#include <vector>
#include <iostream>
#include <fstream>


std::vector<double> vectorOggetto(std::string line)
{
  std::stringstream input_stringstream(line);
  std::string parsed;
  std::vector<double> toReturn;
  if(std::getline(input_stringstream,parsed,','))
  {
      ROS_INFO("\n Parsing di %s \n", parsed.c_str());
  }

  while(std::getline(input_stringstream,parsed,','))
  {
    std::string::size_type sz;
    double tmp = std::stod(parsed,&sz);
    ROS_INFO("\nValore inserito nel vettore: %f\n", tmp);
    toReturn.push_back(tmp);
  }
  return toReturn;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "calcoloTrasformate");
  ros::NodeHandle nh;
  tf::TransformListener t;

  sleep(7);

  std::ifstream fileInput("tfoggetti.txt");
  std::ofstream fileOutput("tfOggTrasformate.txt");
  std::string line;

  while( std::getline(fileInput, line) )
  {
    ROS_INFO("\nCalcolo trasformata di %s\n", line.c_str());
    tf::StampedTransform transform;
           try
           {
             t.waitForTransform("/world", line.c_str(), ros::Time(0), ros::Duration(5.0));
             t.lookupTransform("/world", line.c_str(), ros::Time(0), transform);
             ROS_INFO("\nTrasformata di %s rispetto /world calcolata!\n", line.c_str());
           }
           catch (tf::TransformException &ex)
           {
             ROS_ERROR("%s",ex.what());
             ros::Duration(1.0).sleep();
             continue;
           }
    fileOutput << line.c_str() << "," << transform.getOrigin().getX() << "," <<
                  transform.getOrigin().getY() << "," << transform.getOrigin().getZ()
               << "," << transform.getRotation().getX() << "," << transform.getRotation().getY() << "," <<
                transform.getRotation().getZ() << "," << transform.getRotation().getW()<< "\n";
  }
  fileInput.close();
  fileOutput.close();

  std::ifstream fIn("tfOggTrasformate.txt");
  std::string li;
  std::vector<double> prova;
  while (std::getline(fIn, li))
  {
   prova = vectorOggetto(li);
  }

  for(std::vector<double>::iterator it = prova.begin() ; it != prova.end(); ++it)
  {
    ROS_INFO("\nValori nel vettore prova: %f\n", *it);
  }
}


