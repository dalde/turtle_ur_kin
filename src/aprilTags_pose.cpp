#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <std_msgs/String.h>
#include <turtle_ur_kin/AprilTagDetection.h>
#include <turtle_ur_kin/AprilTagDetectionArray.h>
#include <tf/transform_listener.h>
#include <vector>
#include <iostream>
#include <fstream>

/*
 * ---OGGETTI---
 * ID 0, 1, 2, 3: cubi
 * ID 4, 5:       cilindri
 * ID 6, 7, 8:    prismi
 * */

class Oggetto
{

private:
  std_msgs::String type;
  std::string tipo;
  int id;
  geometry_msgs::Pose pose;

public:
  Oggetto()
  {
    id = 0;
  }

  Oggetto(int a)
  {
    id = a;
    if (id <= 3)
      tipo = "cubo_" + std::to_string(a+1);
    else if (id == 4 || id == 5 )
      tipo = "cilindro_" + std::to_string(a+1);
    else if (id >= 6 || id <=9)
      tipo = "prisma_" + std::to_string(a+1);
    else
      tipo = "id_invalido";

    type.data = tipo;
  }

  void setID(int n)
  {
    id = n;
    if (id <= 3)
      tipo = "cubo_" + std::to_string(n+1);
    else if (id == 4 || id == 5 )
      tipo = "cilindro_" + std::to_string(n+1);
    else if (id >= 6 || id <=9)
      tipo = "prisma_" + std::to_string(n+1);
    else
      tipo = "id_invalido";

    type.data = tipo;
  }

  std::string getTFName()
  {
    std::string tf = "/" + type.data;
    return tf;
  }

  void getInfo()
  {
    ROS_INFO ("\n---------------- \nID : [%d], TIPO: [%s], TF_NAME: [%s]", id, type.data.c_str(), getTFName().c_str());
  }

  std::string getTipo()
  {
    return tipo;
  }

  int getId()
  {
    return id;
  }

  void setPose(geometry_msgs::PoseStamped poseIn)
  {
    pose.position.x = poseIn.pose.position.x;
    pose.position.y = poseIn.pose.position.y;
    pose.position.z = poseIn.pose.position.z;
    pose.orientation.x = poseIn.pose.orientation.x;
    pose.orientation.y = poseIn.pose.orientation.y;
    pose.orientation.z = poseIn.pose.orientation.z;
    pose.orientation.w = poseIn.pose.orientation.w;
  }

  geometry_msgs::Pose getPose()
  {
    return pose;
  }

  void getPoseInfo()
  {
    ROS_INFO( "\nPOSIZIONE = pos_x: [%1.3f], pos_y: [%1.3f], pos_z: [%1.3f] \nORIENTAZIONE = or_x: [%1.3f], or_y: [%1.3f], or_z: [%1.3f], or_w: [%1.3f]",
            pose.position.x, pose.position.y, pose.position.z, pose.orientation.x, pose.orientation.y, pose.orientation.z,
        pose.orientation.w );
  }

};


bool scritto = false;
int vectorSize = 0;
std::string oggRil[9];
double arrayOggRil[9][8];
std::vector<int> contatori = {0,4,6};
int totale = 0;


std::vector<double> vectorOggetto(std::string line)
{
  std::stringstream input_stringstream(line);
  std::string parsed;
  std::vector<double> toReturn;
  if(std::getline(input_stringstream,parsed,','))
  {
      ROS_INFO("\n Parsing di %s \n", parsed.c_str());
  }

  while(std::getline(input_stringstream,parsed,','))
  {
    std::string::size_type sz;
    double tmp = std::stod(parsed,&sz);
    ROS_INFO("\nValore inserito nel vettore: %f\n", tmp);
    toReturn.push_back(tmp);
  }
  return toReturn;
}

void calcolaTrasformata(){
  tf::TransformListener t;

  std::string line;

  for(int i = 0; i < 9; i++)
  {
    if(!oggRil[i].empty()){
      line = oggRil[i];
    ROS_INFO("Calcolo trasformata di %s", line.c_str());
    tf::StampedTransform transform;
           try
           {
             t.waitForTransform("/world", line.c_str(), ros::Time(0), ros::Duration(5.0));
             t.lookupTransform("/world", line.c_str(), ros::Time(0), transform);
             ROS_INFO("Trasformata calcolata!");
           }
           catch (tf::TransformException &ex)
           {
             ROS_ERROR("%s",ex.what());
             ros::Duration(1.0).sleep();
             continue;
           }
    int riga = -1;
    std::string confronto = line.substr(1,line.size()-3);

    if( confronto.compare("cubo") == 0){
      riga = contatori[0];
      contatori[0]++;
    }
    if(confronto.compare("cilindro") == 0){
      riga = contatori[1];
      contatori[1]++;
    }
    if(confronto.compare("prisma") == 0){
      riga = contatori[2];
      contatori[2]++;
    }
    totale++;

    arrayOggRil[riga][0] = 10;
    arrayOggRil[riga][1] = transform.getOrigin().getX();
    arrayOggRil[riga][2] = transform.getOrigin().getY();
    arrayOggRil[riga][3] = transform.getOrigin().getZ();
    arrayOggRil[riga][4] = transform.getRotation().getX();
    arrayOggRil[riga][5] = transform.getRotation().getY();
    arrayOggRil[riga][6] = transform.getRotation().getZ();
    arrayOggRil[riga][7] = transform.getRotation().getW();

    ROS_INFO("TOTALE OGGETTI: %d\n", totale);
    }
  }
  std::fstream file;
  file.open("/home/daniele/.ros/oggettiRilevati.txt", std::fstream::out);
  ROS_INFO("Scrivo su file le tf\n");
   for (int cont = 0; cont < 9; cont++)
   {
     file << arrayOggRil[cont][0] << " " << arrayOggRil[cont][1] << " " << arrayOggRil[cont][2] << " " << arrayOggRil[cont][3] << " "
          << arrayOggRil[cont][4] << " " << arrayOggRil[cont][5] << " " << arrayOggRil[cont][6] << " " << arrayOggRil[cont][7] << "\n";
   }
   file.close();
  /*for(int i1 = 0 ; i1 < 9; i1++)
  {
    ROS_INFO("\n%d %f %f %f %f %f %f %f", i1, arrayOggRil[i1][0], arrayOggRil[i1][1], arrayOggRil[i1][2], arrayOggRil[i1][3],
                                         arrayOggRil[i1][4], arrayOggRil[i1][5], arrayOggRil[i1][6]);
  }*/
}

void aprilCallback(const turtle_ur_kin::AprilTagDetectionArray::ConstPtr& msg)
{
  int countRil = 0;
  ROS_INFO("\n**************** CHIAMATA aprilCallback ****************\n");
  std::vector<Oggetto> arrayOgg;
  for(auto it = msg->detections.begin(); it != msg->detections.end(); ++it) // popolamento vettore di oggetti
  {
   turtle_ur_kin::AprilTagDetection det = *it;
   Oggetto tmp;
   geometry_msgs::PoseStamped psOggetto = det.pose;  // questa è la pose rispetto camera_rgb_optical_frame
   tmp.setPose(psOggetto);
   tmp.setID(det.id);
   tmp.getInfo();
   //tmp.getPoseInfo();
   arrayOgg.push_back(tmp);
  }
  if(vectorSize != arrayOgg.size())
  {
      vectorSize = arrayOgg.size();
      scritto = false;
  }

  if(!scritto)
  {
  std::ofstream file("tfoggetti.txt");
  for (std::vector<Oggetto>::iterator it = arrayOgg.begin() ; it != arrayOgg.end(); ++it)
  {
    ROS_INFO("Rilevata la tf di: %s\n", it->getTFName().c_str());
    oggRil[countRil] = it->getTFName().c_str();
    countRil++;
    //file << it->getTFName() << "\n";
  }
  file.close();
  ROS_INFO("\n\n\nCalcolo le trasformate\n\n\n");
  calcolaTrasformata();
  exit(0);
  }
  scritto = true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "aprilTag_pose");
  ros::NodeHandle nh;
  tf::TransformListener t;
  ros::Subscriber sub = nh.subscribe("tag_detections", 1, aprilCallback);

  if(sub){
    ros::spin();
  }
  else
    ROS_INFO("ERRORE DEL SUBSCRIBER!!!");

}
//FINE FILE APRILTAG
