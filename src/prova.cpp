#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sensor_msgs/LaserScan.h>
#include <ros/callback_queue.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_broadcaster.h>
#include <sstream>

int count = 0;
float destra = 0;
float sinistra = 0;

void recovery(double linear_x, double linear_y, double linear_z, double angular_x, double angular_y, double angular_z, int num_cycles)
{
    ros::NodeHandle nh_;
    ros::Publisher cmd_vel_pub = nh_.advertise<geometry_msgs::Twist>("/turtlebot/mobile_base/commands/velocity", 1);

    geometry_msgs::Twist base_cmd;

    base_cmd.linear.x = linear_x;
    base_cmd.linear.y = linear_y;
    base_cmd.linear.z = linear_z;

    base_cmd.angular.x = angular_x;
    base_cmd.angular.y = angular_y;
    base_cmd.angular.z = angular_z;

    ros::Duration duration(0.1);

    for(unsigned int i = 0; i < num_cycles; ++i) {
        cmd_vel_pub.publish(base_cmd);
        duration.sleep();
    }
}

/*
 * 0 - 683 -> 684
 * centrale 342
 */
void processLaserScan(sensor_msgs::LaserScan scan){
    ROS_INFO("DESTRA: %f - SINISTRA: %f", scan.ranges[140], scan.ranges[540]);
    count++;
    destra += scan.ranges[140];
    sinistra += scan.ranges[540];
}

void lettura(){
  ros::NodeHandle n_lettura;
  ros::Subscriber scanSub;
  scanSub = n_lettura.subscribe<sensor_msgs::LaserScan>("/turtlebot/scan", 100, processLaserScan);
  sinistra = 0;
  destra = 0;
  //ROS SPIN manuale
  while (ros::ok())
  {
    ros::getGlobalCallbackQueue()->callAvailable(ros::WallDuration(0.01));
    if(count == 3)
      break;
  }
  sinistra = sinistra/3;
  destra = destra/3;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "laser");

  ros::NodeHandle nh;
  float rotazione = 0;
  while(true){
      count = 0;

      lettura();

      if((sinistra > 0.25 && sinistra < 0.35) && (destra > 0.25 && destra < 0.35)){
        ROS_INFO("sono al centro");
        rotazione = 0;
      }
      else if(sinistra < 0.25 && destra > 0.35){
        ROS_INFO("mi allontano da sx");
        rotazione = -0.3;
      }
      else if(destra < 0.25  && sinistra > 0.35){
        ROS_INFO("mi allontano da DX");
        rotazione = 0.3;
      }
      else if (sinistra > 0.35 && destra > 1.0){
        ROS_INFO("mi avvicino a SX");
        rotazione = 0.3;
      }
      else if (destra > 0.35 && sinistra > 1.0){
        ROS_INFO("mi avvicino a DX");
        rotazione = -0.3;
      }
      recovery(0.1, 0.0, 0.0, 0.0, 0.0, rotazione, 30);
  }

  return 0;
}


/*
 * Header header
float32 angle_min        # start angle of the scan [rad]
float32 angle_max        # end angle of the scan [rad]
float32 angle_increment  # angular distance between measurements [rad]
float32 time_increment   # time between measurements [seconds]
float32 scan_time        # time between scans [seconds]
float32 range_min        # minimum range value [m]
float32 range_max        # maximum range value [m]
float32[] ranges         # range data [m] (Note: values < range_min or > range_max should be discarded)
float32[] intensities    # intensity data [device-specific units]
*/
