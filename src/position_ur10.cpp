#include "ros/ros.h"
#include <sstream>
#include "geometry_msgs/Pose.h"
#include "moveit/plan_execution/plan_execution.h"
#include "moveit/move_group_interface/move_group_interface.h"
#include "moveit/planning_scene_interface/planning_scene_interface.h"
#include "std_msgs/String.h"
#include "turtle_ur_kin/mgripper.h"
#include <urdf/model.h>
#include <iostream>
#include <fstream>


class Subscribe_And_Publish
{
private:
    ros::Subscriber sub;
    ros::NodeHandle n;
public:
    Subscribe_And_Publish()
    {
      ros::Subscriber sub = n.subscribe ("chatter", 500, &Subscribe_And_Publish::callback, this);
    }

    void callback(const std_msgs::String::ConstPtr& msg)
    {
      ROS_INFO("dentro");
      ROS_INFO("I heard: [%s]", msg->data.c_str());
    }

};


std::vector<int> elementi = {0,0,0};
std::vector<int> portata = {0};
std::vector<int> trasportati = {0,0,0};


void sincroTurtle(ros::Publisher turtle){

  std_msgs::String msg;
  std::stringstream ss;
  ss << "hello world";
  msg.data = ss.str();
  turtle.publish(msg);

   //Subscribe_And_Publish SAPObject;

  ROS_INFO("\n\n\nCONTATTO IL TURTLE\n\n");

}

void apriGripper (ros::Publisher &chatter_pub){
  turtle_ur_kin::mgripper msg;
  msg.rACT = 1;
  msg.rMOD = 0;
  msg.rGTO = 1;
  msg.rATR = 0;
  msg.rGLV = 0;
  msg.rICF = 0;
  msg.rICS = 0;
  msg.rPRA = 0;
  msg.rSPA = 0;
  msg.rFRA = 0;
  msg.rPRB = 0;
  msg.rSPB = 0;
  msg.rFRB = 255;
  msg.rPRC = 0;
  msg.rSPC = 0;
  msg.rFRC = 0;
  msg.rPRS = 0;
  msg.rSPS = 0;
  msg.rFRS = 0;
  ROS_INFO("%s","Messaggio APERTURA pubblicato"); //tipo di dato
  chatter_pub.publish(msg);//pubblicazione del messaggio
  sleep(2);
}

void chiudiGripper (ros::Publisher &chatter_pub){
  turtle_ur_kin::mgripper msg;
  msg.rACT = 1;
  msg.rMOD = 0;
  msg.rGTO = 1;
  msg.rATR = 0;
  msg.rGLV = 0;
  msg.rICF = 0;
  msg.rICS = 0;
  msg.rPRA = 255;
  msg.rSPA = 255;
  msg.rFRA = 150;
  msg.rPRB = 0;
  msg.rSPB = 0;
  msg.rFRB = 0;
  msg.rPRC = 0;
  msg.rSPC = 0;
  msg.rFRC = 0;
  msg.rPRS = 0;
  msg.rSPS = 0;
  msg.rFRS = 0;
  ROS_INFO("%s","Messaggio CHIUSURA pubblicato"); //tipo di dato
  chatter_pub.publish(msg);//pubblicazione del messaggio
  sleep(2);
}

void go_cartesian(int goal, moveit::planning_interface::MoveGroupInterface &move_group,
                 float x, float y, float z, float ox, float oy, float oz, float ow)
{
   geometry_msgs::Pose target_pose;
   target_pose.orientation.x = ox;
   target_pose.orientation.y = oy;
   target_pose.orientation.z = oz;
   target_pose.orientation.w = ow;
   target_pose.position.x = x;
   target_pose.position.y = y;
   target_pose.position.z = z;

   move_group.setPoseTarget(target_pose);

   moveit::planning_interface::MoveGroupInterface::Plan my_plan;
   moveit::planning_interface::MoveItErrorCode success = move_group.plan(my_plan);

   if(success == moveit_msgs::MoveItErrorCodes::SUCCESS){
     switch (goal){
         case 1:
         {
            ROS_INFO_NAMED("START", "-> start");
         }break;
         case 2:
         {
            ROS_INFO_NAMED("DOWN", "-> rilascio oggetto");
         }break;
         case 3:
         {
            ROS_INFO_NAMED("PICK UP", "-> presa oggetto");
         }break;
         case 4:
         {
            ROS_INFO_NAMED("PICK UP", "-> sopra oggetto");
         }break;
         case 5:
         {
            ROS_INFO_NAMED("PICK UP", "-> sopra cestino");
         }break;
     }
     move_group.move();
     move_group.clearPoseTargets();
   }
   else{
     switch (goal){
         case 1:
         {
            ROS_INFO_NAMED("START", "FALLITA -> start");
         }break;
         case 2:
         {
            ROS_INFO_NAMED("DOWN", "FALLITA -> rilascio oggetto");
         }break;
         case 3:
         {
            ROS_INFO_NAMED("PICK UP", "FALLITA -> presa oggetto");
         }break;
         case 4:
         {
            ROS_INFO_NAMED("PICK UP", "FALLITA -> sopra oggetto");
         }break;
         case 5:
         {
            ROS_INFO_NAMED("PICK UP", "FALLITA -> sopra cestino");
         }break;
     }
   }
}

tf::Quaternion trasformPoseCubo(std::vector<double> pose){

  tf::Quaternion qCubo(pose[3], pose[4], pose[5], pose[6]);

  double roll, pitch, yaw;

  tf::Matrix3x3 m(qCubo);
  m.getRPY(roll, pitch, yaw);

  if(yaw <= -2.775)
    yaw = yaw + 3.14;


  tf::Quaternion qEelink = tf::createQuaternionFromRPY(2.398, 1.565, yaw+0.785);


  return qEelink;
}

std::vector<double> trasformPosePrisma(std::vector<double> pose){

  std::vector<double> ritorno = {0, 0, 0, 0, 0, 0, 0};
  tf::Quaternion qCubo(pose[3], pose[4], pose[5], pose[6]);

  double roll, pitch, yaw;

  tf::Matrix3x3 m(qCubo);
  m.getRPY(roll, pitch, yaw);

  if(yaw <= -2.775)
    yaw = yaw + 3.14;

  tf::Quaternion qEelink = tf::createQuaternionFromRPY(2.398, 1.565, yaw+0.785);

  ritorno[3] = qEelink.getX();
  ritorno[4] = qEelink.getY();
  ritorno[5] = qEelink.getZ();
  ritorno[6] = qEelink.getW();

  if(yaw < -1.4 && yaw > -1.85){
    ritorno[0] = pose[0] - 0.019;
    ritorno[1] = pose[1] + 0.017;
    ROS_INFO("dentro_centro");
  }
  else if(yaw >= -1.4){
    ritorno[0] = pose[0] - 0.034;
    ritorno[1] = pose[1] + 0.047;
    ROS_INFO("dentro_park");
  }
  else if(yaw <= -1.85){
    ritorno[0] = pose[0] - 0.033;
    ritorno[1] = pose[1] - 0.03;
    ROS_INFO("dentro_tavolo");
  }
  ritorno[2] = 1.234;

  return ritorno;
}

void posizioneCest(moveit::planning_interface::MoveGroupInterface &move_group){

  ROS_INFO("Posizione di start con IK");
  moveit::core::RobotStatePtr current_state = move_group.getCurrentState();
  const robot_state::JointModelGroup *joint_model_group = move_group.getCurrentState()->getJointModelGroup("manipulator");

  std::vector<double> joint_group_positions;
  current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);


  joint_group_positions[0] = 0.3;// radians
  joint_group_positions[1] = -1.4;// radians
  joint_group_positions[2] = -1.4;// radians
  joint_group_positions[3] = -1.7;// radians
  joint_group_positions[4] = 1.6;// radians
  joint_group_positions[5] = 0;// radians
  joint_group_positions[6] = 1.;// radians

  move_group.setJointValueTarget(joint_group_positions);

  move_group.move();

}

void posizioneStart(moveit::planning_interface::MoveGroupInterface &move_group){

  ROS_INFO("Posizione di start con IK");
  moveit::core::RobotStatePtr current_state = move_group.getCurrentState();
  const robot_state::JointModelGroup *joint_model_group = move_group.getCurrentState()->getJointModelGroup("manipulator");

  std::vector<double> joint_group_positions;
  current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);


  joint_group_positions[0] = 1.8;// radians
  joint_group_positions[1] = -1.4;// radians
  joint_group_positions[2] = -1.4;// radians
  joint_group_positions[3] = -1.7;// radians
  joint_group_positions[4] = 1.6;// radians
  joint_group_positions[5] = 0;// radians
  joint_group_positions[6] = 1.;// radians

  move_group.setJointValueTarget(joint_group_positions);

  move_group.move();

}

void goCestin(moveit::planning_interface::MoveGroupInterface &move_group, ros::Publisher turtle, ros::Publisher chatter_pub)
{
  posizioneStart(move_group);
  posizioneCest(move_group);
  ROS_INFO("Vado al cestino");
  move_group.setNumPlanningAttempts(1);
  std::vector<double> pose;

  if(trasportati[0] == 0 && elementi[0] > 0){
       ROS_INFO("cestino - cilindro1 - port.0");
       pose = {0.586, 0.960, 0.843, 0.024, 0.716, -0.011, 0.698};
       elementi[0]--;
       trasportati[0]++;
   }
  else if(trasportati[0] == 1 && elementi[0] > 0){
       ROS_INFO("cestino - cilindro2 - port.0");
       pose = {0.586, 1.090, 0.843, 0.024, 0.716, -0.011, 0.6984};
       elementi[0]--;
       trasportati[0]++;
  }
  else if(trasportati[1] == 0 && elementi[1] > 0){
       ROS_INFO("cestino - prisma1 - port.0");
       pose = {0.442, 1.007, 0.787, 0.024, 0.716, -0.011, 0.698};
       elementi[1]--;
       trasportati[1]++;
  }
  else if(trasportati[2] == 0 && elementi[2] > 0){
       ROS_INFO("cestino - cubo1 - port.0");
       pose = {0.442, 0.950, 0.836, 0.024, 0.716, -0.011, 0.698};
       elementi[2]--;
       trasportati[2]++;
  }
  else if(trasportati[2] == 1 && elementi[2] > 0){
       ROS_INFO("cestino - cubo2 - port.0");
       pose = {0.442, 1.038, 0.836, 0.024, 0.716, -0.011, 0.698};
       elementi[2]--;
       trasportati[2]++;
  }
  else if(trasportati[1] == 1  && elementi[1] > 0){
       ROS_INFO("cestino - prisma2 - port.1");
       pose = {0.435, 0.954, 0.787, 0.024, 0.716, -0.011, 0.698};
       elementi[1]--;
       trasportati[1]++;
  }
  else if(trasportati[1] == 2 && elementi[1] > 0){
       ROS_INFO("cestino - prisma3 - port.1");
       pose = {0.571, 0.954, 0.787, 0.024, 0.716, -0.011, 0.698};
       elementi[1]--;
       trasportati[1]++;
  }
  else if (trasportati[2] == 2 && elementi[2] > 0){
       ROS_INFO("cestino - cubo3 - port.1");
       pose = {0.454, 1.063, 0.836, 0.024, 0.716, -0.011, 0.698};
       elementi[2]--;
       trasportati[2]++;
  }
  else if(trasportati[2] == 3 && elementi[2] > 0){
       ROS_INFO("cestino - cubo4 - port.1");
       pose = {0.579, 1.063, 0.836, 0.024, 0.716, -0.011, 0.698};
       elementi[2]--;
       trasportati[2]++;
  }



  go_cartesian(5, move_group, pose[0], pose[1], pose[2]+0.550, pose[3], pose[4], pose[5], pose[6]);
  go_cartesian(2, move_group, pose[0], pose[1], pose[2], pose[3], pose[4], pose[5], pose[6]);
  apriGripper(chatter_pub);

  go_cartesian(5, move_group, pose[0], pose[1], pose[2]+0.550, pose[3], pose[4], pose[5], pose[6]);
  posizioneStart(move_group);

 if((trasportati[0] + trasportati[1] + trasportati[2]) == 5){
    sincroTurtle(turtle);
  }

}

void spostaCilindro(std::vector<double> &pose, moveit::planning_interface::MoveGroupInterface &move_group,
                    ros::Publisher chatter_pub, ros::Publisher turtle){

  ROS_INFO("**************************CILINDRO**************");

  apriGripper(chatter_pub);

  std::vector<double> qEelink = {0.708, 0.032, -0.705, 0.034};

  move_group.setNumPlanningAttempts(1);
  go_cartesian(4, move_group, pose[0]-0.046, pose[1]+0.006, pose[2]+0.400, qEelink[0], qEelink[1], qEelink[2], qEelink[3]);

  move_group.setNumPlanningAttempts(1);
  go_cartesian(3, move_group, pose[0]-0.046, pose[1]+0.006, pose[2]+0.204, qEelink[0], qEelink[1], qEelink[2], qEelink[3]);
  chiudiGripper(chatter_pub);

  go_cartesian(4, move_group, pose[0]-0.046, pose[1]+0.006, 1.515, qEelink[0], qEelink[1], qEelink[2], qEelink[3]);

  goCestin(move_group, turtle, chatter_pub);
}

void spostaPrisma(std::vector<double> &pose, moveit::planning_interface::MoveGroupInterface &move_group,
                  ros::Publisher chatter_pub, ros::Publisher turtle){
  ROS_INFO("**************************PRISMA****************");

  apriGripper(chatter_pub);

  std::vector<double> qEelink = trasformPosePrisma(pose);

  move_group.setNumPlanningAttempts(1);
  go_cartesian(4, move_group, qEelink[0], qEelink[1], qEelink[2]+0.250, qEelink[3], qEelink[4], qEelink[5], qEelink[6]);

  move_group.setNumPlanningAttempts(1);
  go_cartesian(3, move_group, qEelink[0], qEelink[1], qEelink[2], qEelink[3], qEelink[4], qEelink[5], qEelink[6]);
  chiudiGripper(chatter_pub);

  go_cartesian(4, move_group, qEelink[0], qEelink[1], 1.515, qEelink[3], qEelink[4], qEelink[5], qEelink[6]);

  goCestin(move_group, turtle, chatter_pub);
}

void spostaCubo(std::vector<double> &pose, moveit::planning_interface::MoveGroupInterface &move_group,
                ros::Publisher chatter_pub, ros::Publisher turtle){

  ROS_INFO("**************************CUBO******************");

  apriGripper(chatter_pub);

  tf::Quaternion qEelink = trasformPoseCubo(pose);

  move_group.setNumPlanningAttempts(1);
  go_cartesian(4, move_group, pose[0]-0.046, pose[1]+0.006, pose[2]+0.400, qEelink.getX(), qEelink.getY(), qEelink.getZ(), qEelink.getW());

  move_group.setNumPlanningAttempts(1);
  go_cartesian(3, move_group, pose[0]-0.046, pose[1]+0.006, pose[2]+0.204, qEelink.getX(), qEelink.getY(), qEelink.getZ(), qEelink.getW());

  chiudiGripper(chatter_pub);

  go_cartesian(4, move_group, pose[0]-0.046, pose[1]+0.006, 1.515, qEelink[0], qEelink[1], qEelink[2], qEelink[3]);

  goCestin(move_group, turtle, chatter_pub);
}

int main(int argc, char **argv)
{
  sleep(5);
  ros::init(argc, argv, "position_ur10"); //inizializzazione del nodo
  ros::NodeHandle n; //fa partire il nodo

  ros::Publisher chatter_pub = n.advertise<turtle_ur_kin::mgripper>("/robotiq_hands/l_hand/SModelRobotOutput", 100);

  ros::Publisher turtle = n.advertise<std_msgs::String>("/sincro", 100);

  double arrayOggRil[9][8];
  std::ifstream file("oggettiRilevati.txt");
  ROS_INFO("Leggo dal file le tf\n");
  std::string line;
  std::vector<int> contatori = {0,0,0}; //0 per cilindri, 1 per prismi, 2 per cubi
  int i;
  for(i = 0; i < 9; i++){
    std::getline(file, line);
    //ROS_INFO("%s", line.c_str());
    std::istringstream iss( line );
    std::string result;
    for(int countElem = 0; countElem < 8; countElem++){
      if( std::getline( iss, result , ' ') ){
        arrayOggRil[i][countElem] = std::stof(result.c_str());
        if(arrayOggRil[i][countElem] == 10){
          switch (i){
          case ( 0 ):
            contatori[2]++;
            break;
          case ( 1 ):
            contatori[2]++;
            break;
          case ( 2 ):
            contatori[2]++;
            break;
          case ( 3 ):
            contatori[2]++;
            break;
          case ( 4 ):
            contatori[0]++;
            break;
          case ( 5 ):
            contatori[0]++;
            break;
          case ( 6 ):
            contatori[1]++;
            break;
          case ( 7 ):
            contatori[1]++;
            break;
          case ( 8 ):
            contatori[1]++;
            break;
          }
        }
      }
    }
  }
  file.close();
  for(int i1 = 0 ; i1 < 9; i1++)
  {
    ROS_INFO("%f %f %f %f %f %f %f %f", arrayOggRil[i1][0], arrayOggRil[i1][1], arrayOggRil[i1][2], arrayOggRil[i1][3],
                                           arrayOggRil[i1][4], arrayOggRil[i1][5], arrayOggRil[i1][6], arrayOggRil[i1][7]);
  }

  ROS_INFO("\n\n\nCUBI: %d\nCILINDRI: %d\nPRISMI: %d\nTOTALE: %d\n\n", contatori[2], contatori[0], contatori[1], (contatori[0] + contatori[1] + contatori[2]));

  if((contatori[0] + contatori[1] + contatori[2]) == 0) return 0;

  //CILINDRO
  elementi[0] = contatori[0];
  //PRISMA
  elementi[1] = contatori[1];
  //CUBI
  elementi[2] = contatori[2];

  //MOVING
  ros::AsyncSpinner spinner(1);
  spinner.start();
  moveit::planning_interface::MoveGroupInterface move_group("manipulator");
  move_group.setMaxVelocityScalingFactor(0.7);
  move_group.setMaxAccelerationScalingFactor(0.7);
  move_group.setGoalOrientationTolerance(0.001);
  move_group.setGoalPositionTolerance(0.001);

  std::vector<double> pose;
  for(int i = 0; i < 9; i++){
    int posFinale = -1;
    int sposta = -1;

      if((portata[0] == 0 || portata[0] == 1) && elementi[0] > 0){
        posFinale = 4 + elementi[0] -1;
        sposta = 0;
        portata[0]++;
      }
      else if(portata[0] == 2 && elementi[1] > 0){
        posFinale = 6 + elementi[1] - 1;
        sposta = 1;
        portata[0]++;
      }
      else if(portata[0] == 3 && elementi[2] > 0){
        posFinale = 0 + elementi[2] - 1;
        sposta = 2;
        portata[0]++;
      }
      else if(portata[0] == 4 && elementi[2] > 0){
        posFinale = 0 + elementi[2] - 1;
        sposta = 2;
        portata[0]++;
      }
      else if(portata[0] == 5 && elementi[1] > 0){
        posFinale = 6 + elementi[1] - 1;
        sposta = 1;
        portata[0]++;
      }
      else if(portata[0] == 6 && elementi[1] > 0){
        posFinale = 6 + elementi[1] - 1;
        sposta = 1;
        portata[0]++;
      }
      else if(portata[0] == 7 && elementi[2] > 0){
        posFinale = 0 + elementi[2] - 1;
        sposta = 2;
        portata[0]++;
      }
      else if(portata[0] == 8 && elementi[2] > 0){
        posFinale = 0 + elementi[2] - 1;
        sposta = 2;
        portata[0]++;
      }

      else portata[0]++;

      pose = {arrayOggRil[posFinale][1], arrayOggRil[posFinale][2], arrayOggRil[posFinale][3], arrayOggRil[posFinale][4],
              arrayOggRil[posFinale][5], arrayOggRil[posFinale][6], arrayOggRil[posFinale][7]};

      move_group.setNumPlanningAttempts(1);
      posizioneStart(move_group);

    if(sposta == 0)
      spostaCilindro(pose, move_group, chatter_pub, turtle);
    else if(sposta == 1)
      spostaPrisma(pose, move_group, chatter_pub, turtle);
    else if(sposta == 2)
      spostaCubo(pose, move_group, chatter_pub, turtle);
    }
  return 0;
}

  //geometry_msgs::PoseStamped current_pose = move_group.getCurrentPose("ee_link");
  //ROS_INFO_STREAM( "Posizione corrente ee_link " << current_pose.pose );


/*
  ROS_INFO("Ora il programma continua eseguendo le operazioni!");

  apriGripper(msg, chatter_pub);
  apriGripper(msg, chatter_pub);
  sleep(4);

  tf::Quaternion qCubo(pose[3], pose[4], pose[5], pose[6]);
  double roll, pitch, yaw;
  tf::Matrix3x3 m(qCubo);
  m.getRPY(roll, pitch, yaw);

  tf::Quaternion qEelink(-0.669, 0.219, 0.676, 0.221);
  double rollee, pitchee, yawee;
  tf::Matrix3x3 mee(qEelink);
  mee.getRPY(rollee, pitchee, yawee);

  //ROS_INFO("INIZIO: Rx, Ry, Rz, Rw = %1.3f  %1.3f  %1.3f %1.3f", q1[0] , q1[1], q1[2], q1[3]);

  qEelink.setRPY(rollee, pitchee, yawee);
*/
  //EXPERIMENT CILINDRO
  /*
  go_cartesian(3, move_group, x, y+0.4, z+0.4, -0.00196408,  0, -0.677671, 0.735362);
  move_group.setNumPlanningAttempts(1);
  go_cartesian(3, move_group, x, y+0.4, z+0.077, -0.00196408,  0, -0.677671, 0.735362);
  go_cartesian(3, move_group, x, y+0.168, z+0.077, -0.00196408,  0, -0.677671, 0.735362);
  */

  /*
   * go_cartesian(3, move_group, pose[0]-0.046, pose[1]+0.006, pose[2]+0.350, qEelink[0], qEelink[1], qEelink[2], qEelink[3]);
  move_group.setNumPlanningAttempts(1);
  go_cartesian(3, move_group, pose[0]-0.046, pose[1]+0.006, pose[2]+0.190, qEelink[0], qEelink[1], qEelink[2], qEelink[3]);
  */
/*
  //EXPERIMENT CUBO
  go_cartesian(3, move_group, pose[0]-0.046, pose[1]+0.006, pose[2]+0.350, qEelink[0], qEelink[1], qEelink[2], qEelink[3]);
  move_group.setNumPlanningAttempts(1);
  go_cartesian(3, move_group, pose[0]-0.046, pose[1]+0.006, pose[2]+0.190, qEelink[0], qEelink[1], qEelink[2], qEelink[3]);
  chiudiGripper(msg, chatter_pub);
  sleep(2);

  //START
  go_cartesian(3, move_group, 0.052, 0.134, 1.549, -0.701, -0.001, 0.713, -0.001);

  //CESTINO CENTRALE SIMULATO
  go_cartesian(2, move_group, 0.535, 1.051, 0.734+0.950, 0.519, -0.487, -0.505, -0.489);
  go_cartesian(2, move_group, 0.535, 1.051, 0.734, 0.519, -0.487, -0.505, -0.489);
  move_group.setNumPlanningAttempts(10);
  apriGripper(msg, chatter_pub);
  sleep(2);

  go_cartesian(2, move_group, 0.535, 1.051, 0.734+0.950, 0.519, -0.487, -0.505, -0.489);
*/

//COLLISION
/*moveit_msgs::CollisionObject collision_object;
collision_object.header.frame_id = move_group.getPlanningFrame();
collision_object.id = "cilindro1";

shape_msgs::SolidPrimitive primitive;
primitive.type = primitive.CYLINDER;
primitive.dimensions.resize(2);
primitive.dimensions[0] = 0.085;
primitive.dimensions[1] = 0.2;

geometry_msgs::Pose c1_pose;
c1_pose.orientation.w = 1.0;
c1_pose.position.x = x;
c1_pose.position.y = y;
c1_pose.position.z = z;

collision_object.primitives.push_back(primitive);
collision_object.primitive_poses.push_back(c1_pose);
collision_object.operation = collision_object.ADD;

std::vector<moveit_msgs::CollisionObject> collision_objects;
collision_objects.push_back(collision_object);

planning_scene_interface.addCollisionObjects(collision_objects);

move_group.attachObject(collision_object.id);*/


//EXPERIMENT CUBO !!!!!!!!BENE!
//go_cartesian(3, move_group, x, y, z+0.35, -0.707, 0, 0.707, 0.001);
//go_cartesian(3, move_group, x, y, z+0.21, -0.707, 0, 0.707, 0.001);

//CUBO SUL TAVOLO x,y,z
//-0.052, -0.003, 0.955

//POSIZIONE ee_link per prendere pezzo x,y,z
//-0.081, 0.014, 1.297

//ALTEZZA PRESA CUBO z += 0,120

//CILINDRO SUL TAVOLO EXP x,y,z
//0.060, 0.026, 0.992

//GRIPPER PRENDE PEZZO
//-0.058, 0.194, 1.069

//CILINDRO SOPRA AL CILINDRO EXP x,y,z
//-0.259, 0.002, 1.246

//PRESA ORIZONTALE CILINDRO 0.168 +y
//ALTEZZA PRESA CILINDRO 0.077 +z


//ORIENTAZIONE GRIPPER ORIZONTALE ox,oy,oz,ow
//-0.00196408,  0.000917761, -0.677671, 0.735362


//POSIZIONE DI START, numero goal = 1
//go_cartesian(1, move_group, 0.107, -0.104, 1.524, -0.700, -0.001, 0.714, 0.001);


/*posiione tag detect pose lori CILINDRO
 position:
      x: -0.273992114136
      y: -0.531094960232
      z: 2.27253911316
    orientation:
      x: 0.956493607531
      y: -0.175262556364
      z: 0.0218493326767
      w: 0.232218909116
  go_cartesian(3, move_group, -0.210848, 0.0807884, 1.33362, 0.620861, 0.305317, -0.632481, 0.348255);


posizione CILINDRO ee_link VERTICALE

 x: -0.210848
  y: 0.0807884
  z: 1.33362
orientation:
  x: 0.620861
  y: 0.305317
  z: -0.632481
  w: 0.348255

  posizione CILINDRO ee_link ORIZIONTALE

 y: 0.260726
  z: 1.13053
orientation:
  x: 0.101856
  y: 0.029313
  z: -0.873175
  w: 0.475743

posizione cestino centrale SIMULATO
x: 0.389203
  y: 0.948398
  z: 0.745507
orientation:
  x: -0.699567
  y: -0.000972813
  z: 0.714566
  w: 0.000773572
0.389203, 0.948398, 0.745507, -0.699567, -0.000972813, 0.714566, 0.000773572

posizione cestino centrale REALE
x: 0.534472
  y: 0.983755
  z: 0.801247
orientation:
  x: 0.708969
  y: -0.0100371
  z: -0.704715
  w: 0.0252694

posizione cestino destra
x: 0.615308
  y: 0.980045
  z: 0.799538
orientation:
  x: 0.705988
  y: 0.0122674
  z: -0.706275
  w: 0.0510557

posizione cestino sinistra
 x: 0.46197
  y: 0.97496
  z: 0.798545
orientation:
  x: 0.710823
  y: -0.0333704
  z: -0.702554
  w: 0.00594317
*/
//tf::Vector3 assi = q1.getAxis();
//q1.setX(assi.getX() + 10);

/*tf::Quaternion q1(0.504, -0.214, 0.119, 1.358);
double roll, pitch, yaw;
tf::Matrix3x3 m(q1);
m.getRPY(roll, pitch, yaw);

ROS_INFO("INIZIO: Rx, Ry, Rz, Rw = %1.3f  %1.3f  %1.3f %1.3f", q1[0] , q1[1], q1[2], q1[3]);

q1.setRPY(roll, pitch + 0.1, yaw);

ROS_INFO("FINE  : Rx, Ry, Rz, Rw = %1.3f  %1.3f  %1.3f %1.3f", q1[0] , q1[1], q1[2], q1[3]);
//POSIZIONE DI START MODIFICATA, numero goal = 1
//go_cartesian(1, move_group, target_pose,-0.470, 0.514, 0.511, q1[0] , q1[1], q1[2], q1[3]);
*/
