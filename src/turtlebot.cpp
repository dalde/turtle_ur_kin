#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/LaserScan.h>
#include <boost/bind.hpp>

// ####################################################################################################################
// ####################################################################################################################

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

// ####################################################################################################################
// ####################################################################################################################

void unstuck(int);
void poseEstimation(int);
void recovery(double, double, double, double, double, double, int);
bool planner(move_base_msgs::MoveBaseGoal);
move_base_msgs::MoveBaseGoal setParamGoal(double, double, double);
void processLaserScan(sensor_msgs::LaserScan, int);
double laserScan(int);

// ####################################################################################################################
// ####################################################################################################################

double laserValue = 0.0;
int countSpin = 0;

// ####################################################################################################################
// ####################################################################################################################

int main(int argc, char** argv){
    ros::init(argc, argv, "navigation_goals");

    bool succeeded = false;
    double x, y, orintation_z;
    char *poseEstimationFlag;
    move_base_msgs::MoveBaseGoal dockInit, dockEnd, dockStart, dockTunel;
    int centralBeam = 342;

    dockInit = setParamGoal(-1.358, 0.802, 0);
    dockEnd = setParamGoal(0.021, -0.224, -1.159);
    dockTunel = setParamGoal(0.196, 2.033, 1.954);
    dockStart = setParamGoal(-2.958, 0.617, -1.170);

    poseEstimationFlag = argv[1];
    x = atoi(argv[2]);
    y = atoi(argv[3]);
    orintation_z = atof(argv[4]);

    if(*poseEstimationFlag == 't') {
        poseEstimation(90);
    } else {

//        while (succeeded != true) {
//
//            succeeded = planner(setParamGoal(x,y,orintation_z));
//
//            if (succeeded == true) {
//                std::cout<<"LASER: " << laserScan() <<std::endl;
//            } else {
//                unstuck(15);
//            }
//        }

        ros::Duration duration(0.2);

        while(true)
        {
            std::cout<<"LASER: " << laserScan(centralBeam) <<std::endl;
            duration.sleep();
        }

//        int count = 0;
//        move_base_msgs::MoveBaseGoal docks[] = {dockTunel, dockEnd};
//
//        while (count < 2) {
//            succeeded = false;
//            while (succeeded != true) {
//                succeeded = planner(docks[count]);
//                if (succeeded == true) {
//                    std::cout<<"LASER: " << laserScan() <<std::endl;
//                    count++;
//                } else {
//                    unstuck(8);
//                }
//            }
//        }
    }

    return 0;
}

// ####################################################################################################################
// ####################################################################################################################

bool planner(move_base_msgs::MoveBaseGoal goal)
{
    MoveBaseClient ac("move_base", true);

    while(!ac.waitForServer(ros::Duration(5.0))){
        ROS_INFO("Waiting for the move_base action server");
    }

    std::cout<<"Sending goal"<<std::endl;
    ac.sendGoal(goal);

    ac.waitForResult();
//    ROS_INFO("STATE: %s", ac.getState().getText().c_str());

    if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
        return true;
    }

    return false;
}

// ####################################################################################################################
// ####################################################################################################################

void poseEstimation(int num_cycles)
{
    recovery(0.0, 0.0, 0.0, 0.0, 0.0, 1.3, num_cycles);
}

void unstuck(int num_cycles)
{
    recovery(0.0, 0.0, 0.0, 0.0, 0.0, 2.0, num_cycles);
}

// ####################################################################################################################
// ####################################################################################################################

void processLaserScan(sensor_msgs::LaserScan scan, int beam)
{
    laserValue += scan.ranges[beam];
    countSpin++;
}

// ####################################################################################################################
// ####################################################################################################################

double laserScan(int beam)
{
    ros::NodeHandle nh_;
    ros::Subscriber scanSub;
    int numberScans = 10;

    scanSub = nh_.subscribe<sensor_msgs::LaserScan>("/turtlebot/scan", 100, boost::bind(&processLaserScan, _1, beam));

    countSpin = 0;
    laserValue = 0;

    //ROS SPIN manuale
    while (ros::ok())
    {
        ros::getGlobalCallbackQueue()->callAvailable(ros::WallDuration(0.01));
        if(countSpin >= numberScans)
            break;
    }

    return laserValue/numberScans;
}

// ####################################################################################################################
// ####################################################################################################################

void recovery(double linear_x, double linear_y, double linear_z, double angular_x, double angular_y, double angular_z, int num_cycles)
{
    ros::NodeHandle nh_;
    ros::Publisher cmd_vel_pub = nh_.advertise<geometry_msgs::Twist>("/turtlebot/mobile_base/commands/velocity", 1);

    geometry_msgs::Twist base_cmd;

    base_cmd.linear.x = linear_x;
    base_cmd.linear.y = linear_y;
    base_cmd.linear.z = linear_z;

    base_cmd.angular.x = angular_x;
    base_cmd.angular.y = angular_y;
    base_cmd.angular.z = angular_z;

    ros::Duration duration(0.1);

    for(unsigned int i = 0; i < num_cycles; ++i) {
        cmd_vel_pub.publish(base_cmd);
        duration.sleep();
    }
}

// ####################################################################################################################
// ####################################################################################################################

move_base_msgs::MoveBaseGoal setParamGoal(double x, double y, double orientation_z)
{
    move_base_msgs::MoveBaseGoal goal;

    ros::Time::init();
    tf::Quaternion q = tf::createQuaternionFromRPY(0, 0, orientation_z);

    goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = ros::Time::now();

    goal.target_pose.pose.position.x = x;
    goal.target_pose.pose.position.y = y;

    goal.target_pose.pose.orientation.x = q.getX();
    goal.target_pose.pose.orientation.y = q.getY();
    goal.target_pose.pose.orientation.z = q.getZ();
    goal.target_pose.pose.orientation.w = q.getW();

    return  goal;
}