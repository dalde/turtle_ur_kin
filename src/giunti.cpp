#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "moveit/plan_execution/plan_execution.h"
#include "moveit/move_group_interface/move_group_interface.h"
#include "turtle_ur_kin/mgripper.h"

int main(int argc, char **argv)
{

  ros::init(argc, argv, "position_ur10");

  ros::AsyncSpinner spinner(1);
  spinner.start();

  moveit::planning_interface::MoveGroupInterface move_group("manipulator");

  // GIUNTI
  moveit::core::RobotStatePtr current_state = move_group.getCurrentState();
  const robot_state::JointModelGroup *joint_model_group = move_group.getCurrentState()->getJointModelGroup("manipulator");

  std::vector<double> joint_group_positions;
  current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);


  joint_group_positions[0] = 0.3;// radians
  joint_group_positions[1] = -1.4;// radians
  joint_group_positions[2] = -1.4;// radians
  joint_group_positions[3] = -1.7;// radians
  joint_group_positions[4] = 1.6;// radians
  joint_group_positions[5] = 0;// radians
  joint_group_positions[6] = 1.;// radians

  move_group.setJointValueTarget(joint_group_positions);

  move_group.move();

}
