DIMENSIONI OGGETTI

CUBO: base: 9.5 x 9.5 - altezza: 10
PRISMA: base: 13.5 x 10 - altezza: 6,5
CILINDRO: lato: 5 - raggio: 8.5 - altezza: 20
--------------------------------------------------------------------	

GRIPPER:
Per provare il codice del gripper che si chiude e si apre in loop digitare:
	[1] rosrun turtle_ur_kin mGripper

CHIUDERE IL GRIPPER DA TERMINALE    [1] rostopic pub --once left_hand/command robotiq_s_model_articulated_msgs/SModelRobotOutput {1,1,1,0,0,0,255,255,0,155,0,0,255,0,0,0,0,0}
APRIRE IL GRIPPER DA TERMINALE:	    [1] rostopic pub --once left_hand/command robotiq_s_model_articulated_msgs/SModelRobotOutput {1,2,1,0,0,0,0,255,0,155,0,0,255,0,0,0,0,0}

--------------------------------------------------------------------

KINECT (APRILTAGS):
    controllate di aver installato questa roba prima che era scritta nel pdf della challenge:
        sudo​ ​apt-get​ ​install​ ​ros-kinetic-openni-launch
        git​ ​clone​ ​https://github.com/JenniferBuehler/common-sensors.git
    [1] installare apriltags con: git clone https://github.com/RIVeR-Lab/apriltags_ros.git
    [2] in simulato (dopo aver lanciato i 3 comandi di "SIMULATORE"): roslaunch turtle_ur_kin april_tags.launch
    [3] se volete vedere su RViz fate "Add", sotto by topic scegliete "tag_detections_image" e scegliete image dopo aver messo
        qualche cubo o robe sul tavolino
    [4] con "rostopic echo /tag_detections_pose" ho visto che stampa le x,y,z e quaternioni per i tag che rileva e bisogna capire
        come "prelevarli" da codice

--------------------------------------------------------------------

REALE:
    [1] roslaunch challenge_arena challege_real.launch
    [2] roslaunch ur10_platform_challenge_moveit_config ur10_platform_challenge_moveit_planning_execution.launch
    [3] roslaunch turtel_ur_kin ur10.launch
	
SIMULATORE:
    [1] roslaunch challenge_arena challenge.launch &
    [2] roslaunch ur10_platform_challenge_moveit_config ur10_platform_challenge_moveit_planning_execution.launch sim:=true &
    [3] roslaunch ur10_platform_challenge_moveit_config moveit_rviz.launch config:=true &
    
PIANIFICARE MOVIMENTO BRACCIO RVIZ:
    [1] Planning Request
        [1.1] Plannig Group -> manipulator
    [2] MotionPlannig
        [2.1] Spostamento col cursore del robot
        [2.2] Plan
        [2.3] Execute
        
--------------------------------------------------------------------
Lista TF:
    [1] rosrun tf tf_echo /world /ee_link
    [2] rosrun tf2_tools view_frames.py

--------------------------------------------------------------------
COMPILAZIONE:
    Per compilare solo un pkg digitare: catkin_make --pkg PKG_NAME

CREAZIONE PKG:
    [1] lanciare da terminale "catkin_create_pkg turtle_ur_kin"
    [2] scaricare questa repo in /ros_ws/src/
    [3] eseguire il "catkin_make"
    [4] lanciare da terminale dopo aver fatto partire "roscore"
        -> rosrun turtle_ur_kin ex1_a
